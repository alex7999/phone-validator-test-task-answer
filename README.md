# Mobile phone number validator

This application is a quite simple test task implementation. 

Given a phone number validator that as an input parameter accepts list of phone numbers. 

The validator returns valid numbers grouped by countries and invalid as a separate list.

## Restrictions

No regular expressions except String.replaceAll or third party libraries are allowed except test libraries for assertions if standard junit 4.x in not enough.

## Current behaviour

Phone number may contain plus sign in the beginning, spaces, braces (if there is opening brace there should
 be closing), dashes, eg:
* 37061234567
* +37061234567
* +370(6)1234567
* +370 612 34 567
* +370-612-34-567

Currently supported counties:
* Lithuania (LT ISO country code) (370)
* Latvia (LV ISO country code) (371)
* Estonia (EE ISO country code) (372)
* Belgium (BE ISO country code) (32)

No other symbols except mentioned ones are expected to be passed (e.g. no letters, not #±~, etc), no null values are passed, no empty strings.

### Lithuania

Mobile phone number starts with 6 and total has 8 digits excluding phone calling code, e.g. `3706XXXXXXX`.

### Latvia

Mobile phone number starts with 2 and total has 8 digits excluding phone calling code, e.g. `3712XXXXXXX`.

### Estonia

Mobile phone number starts with 5 and total has 7 or 8 digits excluding phone calling code, e.g. `3725XXXXXX` or `3725XXXXXXX`.

### Belgium 

Mobile phone number start with `456`, `47`, `48` or `49` and total have 9 digits. e.g. `32456XXXXXX` and `+3247XXXXXXX`.

## Was done

A task was completed to add functionality that allows validating Belgian phone numbers.

During the execution of the task, the code was refactored. It was decided to move the logic of validating the phone number of each country into 
a separate class. This made it possible to adhere to the principles of solid, in particular the principle of sole responsibility, and the Open Cose principe.
It is better to have many simple classes than one class with many methods.
Instead of multiple condition checks, a call in the cycle of validators of various countries was implemented. This will allow in the future not to add many more 
conditions, but only to create a new validation script class and add it to the list of available validators (countryPhoneValidators variable 
in the MobilePhoneNumberValidator class).

The validation script itself has been made more versatile, and now it is enough to pass only 4 parameters to execute the validation script for a new country. 
These parameters are located in the Validator classes that implement the CountryPhoneValidator interface.

Before the refactoring, tests were written to check valid and invalid phone numbers for each country, as a result, errors in the implementation of scripts 
were identified and fixed.