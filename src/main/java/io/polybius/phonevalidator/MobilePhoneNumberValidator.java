package io.polybius.phonevalidator;

import io.polybius.phonevalidator.dto.ValidationResultDto;
import io.polybius.phonevalidator.validator.BelgiumPhoneValidator;
import io.polybius.phonevalidator.validator.CountryPhoneValidator;
import io.polybius.phonevalidator.validator.EstoniaPhoneValidator;
import io.polybius.phonevalidator.validator.LatviaPhoneValidator;
import io.polybius.phonevalidator.validator.LithuaniaPhoneValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code MobilePhoneNumberValidator}
 * The validator returns valid numbers grouped by countries and invalid as a separate list.
 */

public class MobilePhoneNumberValidator {

  private final List<CountryPhoneValidator> countryPhoneValidators = List.of(
          new LithuaniaPhoneValidator(),
          new LatviaPhoneValidator(),
          new EstoniaPhoneValidator(),
          new BelgiumPhoneValidator());

  /**
   * Validates phone numbers passed in the list of parameters,
   * groups valid numbers by country,
   * puts invalid phone numbers in the list of invalid numbers
   *
   * @param phoneNumbers {list of phone numbers}
   * @return {ValidationResultDto}
   */

  public ValidationResultDto validate(List<String> phoneNumbers) {

    ValidationResultDto result = new ValidationResultDto();

    for (String phoneNumber : phoneNumbers) {
      boolean isValid = false;
      String country = null;

      for (CountryPhoneValidator countryPhoneValidator : countryPhoneValidators) {
        isValid = countryPhoneValidator.validate(phoneNumber);
        if (isValid) {
          country = countryPhoneValidator.getCountryIsoCode();
          break;
        }
      }
      addPhoneToResult(result, phoneNumber, isValid, country);
    }

    return result;
  }

  private void addPhoneToResult(ValidationResultDto result, String phoneNumber, boolean isValid, String country) {

    if (isValid) {
      addPhoneToValidList(result, phoneNumber, country);
    } else {
      result.getInvalidPhones().add(phoneNumber);
    }
  }

  private void addPhoneToValidList(ValidationResultDto result, String phoneNumber, String country) {

    addNewCountryIfNotPresent(result, country);
    result.getValidPhonesByCountry().get(country).add(phoneNumber);
  }

  private void addNewCountryIfNotPresent(ValidationResultDto result, String country) {

    if (!result.getValidPhonesByCountry().containsKey(country)) {
      result.getValidPhonesByCountry().put(country, new ArrayList<>());
    }
  }


}
