package io.polybius.phonevalidator.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DTO for storing a list of country-grouped valid phone numbers
 * and a list of invalid phone numbers
 */

public class ValidationResultDto {
  private final Map<String, List<String>> validPhonesByCountry = new HashMap<>();
  private final List<String> invalidPhones = new ArrayList<>();

  public Map<String, List<String>> getValidPhonesByCountry() {
    return validPhonesByCountry;
  }

  public List<String> getInvalidPhones() {
    return invalidPhones;
  }
}
