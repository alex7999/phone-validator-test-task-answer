package io.polybius.phonevalidator.validator;

import java.util.List;

public class BelgiumPhoneValidator implements CountryPhoneValidator {
    private static final String COUNTRY_ISO_CODE = "BE";
    private static final String COUNTRY_CODE = "32";
    private static final List<Integer> LENGTH_NUMBER = List.of(9);
    private static final List<String> START_DIGITS = List.of("456", "47", "48", "49");


    @Override
    public String getCountryIsoCode() {
        return COUNTRY_ISO_CODE;
    }

    @Override
    public boolean validate(String phoneNumber) {
        return validate(phoneNumber, COUNTRY_CODE, START_DIGITS, LENGTH_NUMBER);
    }







}
