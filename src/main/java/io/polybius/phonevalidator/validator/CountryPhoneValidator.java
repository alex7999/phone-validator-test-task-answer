package io.polybius.phonevalidator.validator;

import java.util.List;

public interface CountryPhoneValidator {
    String getCountryIsoCode();

    boolean validate(String phoneNumber);

    /**
     * universal method for validating a country phone number
     *
     * @param  phoneNumber  phone number
     * @param  countryCode country code
     * @param  startLetters list of initial characters of phone number without country code
     * @param  lengthPhones list of possible phone number lengths without country code
     * @return      phone number is correct
     */
    default boolean validate(String phoneNumber, String countryCode, List<String> startLetters, List<Integer> lengthPhones) {
        boolean isValid = false;
        int lengthCode = countryCode.length();
        if (phoneNumber.startsWith("+")) {
            phoneNumber = phoneNumber.substring(1);
        }

        if (!phoneNumber.startsWith(countryCode)) {
            return false;
        }

        String phoneNumberWithoutCode = phoneNumber.substring(lengthCode);

        // length check
        if (!lengthPhones.contains(phoneNumberWithoutCode.length())) {
            return false;
        }

        // checking for first characters
        for (String currentFirstLetter : startLetters) {
            if (phoneNumberWithoutCode.startsWith(currentFirstLetter)){
                isValid = true;
            }
        }

        return isValid;
    }

}
