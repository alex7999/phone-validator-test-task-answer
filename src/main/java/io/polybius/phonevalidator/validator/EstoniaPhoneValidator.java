package io.polybius.phonevalidator.validator;

import io.polybius.phonevalidator.validator.CountryPhoneValidator;

import java.util.List;

public class EstoniaPhoneValidator implements CountryPhoneValidator {

    private static final String COUNTRY_ISO_CODE = "EE";
    private static final String COUNTRY_CODE = "372";
    private static final List<Integer> LENGTH_NUMBER = List.of(7, 8);
    private static final List<String> START_DIGITS = List.of("5");

    @Override
    public String getCountryIsoCode() {
        return COUNTRY_ISO_CODE;
    }

    @Override
    public boolean validate(String phoneNumber) {
        return validate(phoneNumber, COUNTRY_CODE, START_DIGITS, LENGTH_NUMBER);
    }
}
