package io.polybius.phonevalidator.validator;

import io.polybius.phonevalidator.validator.CountryPhoneValidator;

import java.util.List;

public class LatviaPhoneValidator implements CountryPhoneValidator {

    private static final String COUNTRY_ISO_CODE = "LV";
    private static final String COUNTRY_CODE = "371";
    private static final List<Integer> LENGTH_NUMBER = List.of(8);
    private static final List<String> START_DIGITS = List.of("2");

    @Override
    public String getCountryIsoCode() {
        return COUNTRY_ISO_CODE;
    }

    @Override
    public boolean validate(String phoneNumber) {
        return validate(phoneNumber, COUNTRY_CODE, START_DIGITS, LENGTH_NUMBER);
    }
}
