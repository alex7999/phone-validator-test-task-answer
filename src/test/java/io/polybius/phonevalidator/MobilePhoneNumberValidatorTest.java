package io.polybius.phonevalidator;

import io.polybius.phonevalidator.dto.ValidationResultDto;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MobilePhoneNumberValidatorTest {

  private final MobilePhoneNumberValidator validator = new MobilePhoneNumberValidator();

  @Test
  public void validate() {
    ValidationResultDto result = validator.validate(List.of("+37061234567"));
    assertEquals(List.of("+37061234567"), result.getValidPhonesByCountry().get("LT"));

    result = validator.validate(List.of("+37091234567"));
    assertEquals(List.of("+37091234567"), result.getInvalidPhones());

    result = validator.validate(List.of("+3706123456"));
    assertEquals(List.of("+3706123456"), result.getInvalidPhones());

  }

  @Test
  public void validatePhoneNumber_Lithuania_expectingSuccess() {
    ValidationResultDto result = validator.validate(List.of("+37061234567", "37061234567", "37051234567"));
    assertEquals(List.of("+37061234567", "37061234567"), result.getValidPhonesByCountry().get("LT"));
    assertEquals(List.of("37051234567"), result.getInvalidPhones());

    result = validator.validate(List.of("+37031234567", "3706123456", "370512345678"));
    assertEquals(List.of("+37031234567", "3706123456", "370512345678"), result.getInvalidPhones());

  }

  @Test
  public void validatePhoneNumber_Latvia_expectingSuccess() {
    ValidationResultDto result = validator.validate(List.of("+37121234567", "37121234567", "37151234567"));
    assertEquals(List.of("+37121234567", "37121234567"), result.getValidPhonesByCountry().get("LV"));
    assertEquals(List.of("37151234567"), result.getInvalidPhones());

    result = validator.validate(List.of("+37131234567", "3712123456", "371212345678"));
    assertEquals(List.of("+37131234567", "3712123456", "371212345678"), result.getInvalidPhones());

  }

  @Test
  public void validatePhoneNumber_Estonia_expectingSuccess() {
    ValidationResultDto result = validator.validate(List.of("+3725123456", "+37251234567", "37251234567"));
    assertEquals(List.of("+3725123456", "+37251234567", "37251234567"), result.getValidPhonesByCountry().get("EE"));

    result = validator.validate(List.of("+37261234567", "37261234567", "372512345678", "372512345"));
    assertEquals(List.of("+37261234567", "37261234567", "372512345678", "372512345"), result.getInvalidPhones());

  }

  @Test
  public void validatePhoneNumber_BelgiumFirstDigits_expectingSuccess() {

    ValidationResultDto result = validator.validate(List.of("+32456123456"));
    assertEquals(List.of("+32456123456"), result.getValidPhonesByCountry().get("BE"));

    result = validator.validate(List.of("+32476123456", "372512345678"));
    assertEquals(List.of("+32476123456"), result.getValidPhonesByCountry().get("BE"));

    result = validator.validate(List.of("+32486123456"));
    assertEquals(List.of("+32486123456"), result.getValidPhonesByCountry().get("BE"));

    result = validator.validate(List.of("+32496123456"));
    assertEquals(List.of("+32496123456"), result.getValidPhonesByCountry().get("BE"));

  }

  @Test
  public void validatePhoneNumber_Belgium_expectingInvalidNumber() {

    ValidationResultDto result = validator.validate(List.of("+32457123456"));
    assertEquals(List.of("+32457123456"), result.getInvalidPhones());

    result = validator.validate(List.of("+32447123456"));
    assertEquals(List.of("+32447123456"), result.getInvalidPhones());

    result = validator.validate(List.of("+324561234561"));
    assertEquals(List.of("+324561234561"), result.getInvalidPhones());

    result = validator.validate(List.of("+3245612345"));
    assertEquals(List.of("+3245612345"), result.getInvalidPhones());
  }

}
